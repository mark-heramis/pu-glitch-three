console.clear()
var texture
var goWild = false
var message = "THE VOICES IN MY HEAD\n         MADE ME DO IT";
var loader = new THREE.FontLoader();
function init() {
  var scene = new THREE.Scene()
  var camera = new THREE.PerspectiveCamera(
    45, window.innerWidth / window.innerHeight, 0.1, 1000
  )

  var webGLRenderer = new THREE.WebGLRenderer()
  webGLRenderer.setClearColor(new THREE.Color(0x000000))
  webGLRenderer.setSize(window.innerWidth, window.innerHeight)
  webGLRenderer.shadowMap.enabled = true
  

  loader.load('fonts/helvetiker_regular.typeface.json', function (font) {
    var xMid, text;
    var color = 0xffffff;
    var matLite = new THREE.MeshBasicMaterial({
      color: color,
      transparent: true,
      opacity: 0.4,
      side: THREE.DoubleSide
    });
    
    /**
     * string text - the text that is going to rendered
     * int size - the font-size of the text that will be rendered.
     */
    
    var shapes = font.generateShapes(message, 40);
    var geometry = new THREE.ShapeBufferGeometry(shapes);
    geometry.computeBoundingBox();
    xMid = - 0.5 * (geometry.boundingBox.max.x - geometry.boundingBox.min.x);
    geometry.translate(xMid, 100, 0);
    // make shape ( N.B. edge view not visible )
    text = new THREE.Mesh(geometry, matLite);
    text.position.z = - 50;
    scene.add(text);
    // make line shape ( N.B. edge view remains visible )
    var holeShapes = [];
    for (var i = 0; i < shapes.length; i++) {
      var shape = shapes[i];
      if (shape.holes && shape.holes.length > 0) {
        for (var j = 0; j < shape.holes.length; j++) {
          var hole = shape.holes[j];
          holeShapes.push(hole);
        }
      }
    }
    shapes.push.apply(shapes, holeShapes);
    var lineText = new THREE.Object3D();
    scene.add(lineText);
  })

  camera.position.x = 0
  camera.position.y = 0
  camera.position.z = 800
  camera.lookAt(new THREE.Vector3(0, 0, 0))
  var orbitControls = new THREE.OrbitControls(camera)
  orbitControls.autoRotate = false
  var clock = new THREE.Clock()
  var ambi = new THREE.AmbientLight(0x181818)
  scene.add(ambi)

  var spotLight = new THREE.DirectionalLight(0xffffff)
  spotLight.position.set(550, 100, 550)
  spotLight.intensity = 0.6
  scene.add(spotLight)

  document.getElementById('WebGL-output').appendChild(webGLRenderer.domElement)
  var renderPass = new THREE.RenderPass(scene, camera)
  var effectGlitch = new THREE.GlitchPass(0)
  effectGlitch.renderToScreen = true
  effectGlitch.goWild = goWild
  
  console.log(effectGlitch);

  var composer = new THREE.EffectComposer(webGLRenderer)
  composer.addPass(renderPass)
  composer.addPass(effectGlitch)
  
  render()
  function render() {
    var delta = clock.getDelta()
    effectGlitch.goWild = goWild
    orbitControls.update(delta)
    requestAnimationFrame(render)
    composer.render(delta)
  }
}

var video = document.getElementById('video');
var canvas = document.getElementById('motion');
var camDiff = 0;

DiffCamEngine.init({
  video: video,
  motionCanvas: canvas,
  initSuccessCallback: () => {
    DiffCamEngine.start();
  },
  initErrorCallback: () => {
    alert('Something went wrong.');
  },
  captureCallback: (payload) => {
    camDiff = payload.score;
  }
});


window.onload = init